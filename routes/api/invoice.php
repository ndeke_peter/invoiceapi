<?php

use App\Http\Controllers\API\v1\InvoiceController;

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/', [InvoiceController::class, 'index']);
    Route::post('/', [InvoiceController::class, 'store']);
});
