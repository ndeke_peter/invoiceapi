<?php

use App\Helpers\ApiResponse;
use App\Http\Controllers\API\v1\Auth\LoginController;
use App\Http\Controllers\API\v1\HomeController;

Route::prefix('v1')->group(function () {
    //Authentication
    Route::get('/', [HomeController::class, 'index']);
    Route::post('login', [LoginController::class, 'authenticate']);

});


Route::fallback(function () {
    return ApiResponse::responseError(
        [
            'Device Info' => request()->header('User-Agent') ?? '',
            'Your IP' => request()->ip() ?? ''
        ],
        'Page Not Found. If error persists, contact us',
        404
    );
});
