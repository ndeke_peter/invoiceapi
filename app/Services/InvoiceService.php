<?php

namespace App\Services;

use App;
use App\Models\Invoice;
use App\Dto\Request\Invoice\CreateDto;
use App\Helpers\RandomNumber;
use App\Models\InvoiceItem;
use App\Models\User;

class InvoiceService 
{
    public function getAllInvoice()
    { 
        $allInvoice = Invoice::orderBy('created_at', 'DESC')->get();
        $allInvoiceCount = Invoice::count();
        $totalInvoiceAmount = InvoiceItem::sum('amount');
        return [
            'invoices' => $allInvoice,
            'invoice_count' => $allInvoiceCount,
            'total_invoice_amount' => $totalInvoiceAmount
        ];
    }

    public function store(User $user,CreateDto $dto): Invoice
    {
        $percentageTax = 7.5;
        $createdInvoice = Invoice::create([
            'invoice_number' => 'INV-'. RandomNumber::generateTransactionRef(),
            'due_date' => $dto->due_date,
            'user_id' => $user->id,
            'company_id' => $dto->company_id,
            'client_id'  => $dto->client_id,
            'description'=>$dto->description,
        ]);
         foreach ($dto->invoice_items as $key => $value) {
             $invoiceItems = $createdInvoice->invoice_items()->create([
               'item_name'       => $value['item_name'],
               'item_description' => $value['item_description'],
               'quantity' => $value['quantity'],
               'price' => $value['price'],
               'amount' => $value['quantity'] * $value['price']
             ]);
         }
         $createdInvoice->tax = ($percentageTax / 100) * $invoiceItems->amount;
         $createdInvoice->total_amount = $createdInvoice->invoice_items()->sum('amount');
         $createdInvoice->save();
         
        return Invoice::where('id', $createdInvoice->id)->first();
    }

}
