<?php

namespace App\Services;

use App\Dto\Value\User\UserServiceResponseDto;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use Auth;

class UserService
{
    // login a user
    public function login($request): UserServiceResponseDto
    {
        if (is_numeric($request->get('email'))) {
            $credentials = [
                'phone' => $request->get('email'),
                'password' => $request->get('password')
            ];
        } elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];
        } else {
            $credentials = $request->only('email', 'password');
        }

        if (!Auth::attempt($credentials)) {
            return new UserServiceResponseDto(false, "Wrong Login Credentials");
        }
        Log::info('User Logins in '. print_r($credentials,true));
        $user = $request->user();
        if ($user->email_verified_at === null) {
            return new UserServiceResponseDto(false, "Please verify your account");
        }

        $tokenResult = $user->createToken('authToken')->plainTextToken;

        $userInfo = User::where('id', $user->id)->first();

        return new UserServiceResponseDto(true, 'User logged in', $userInfo->toArray(), $tokenResult, 'Bearer');
    }
  
     // find  a g user
    public function findOne($conditions): UserServiceResponseDto
    {
        $user = User::where($conditions)->first();
        if (!$user) {
            return new UserServiceResponseDto(false, "No account found");
        }
        
        return new UserServiceResponseDto(true, "Account details", $user->toArray());
    }


}
