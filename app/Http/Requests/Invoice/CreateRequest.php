<?php

namespace App\Http\Requests\Invoice;

use Illuminate\Foundation\Http\FormRequest;
use App\Dto\Request\Invoice\CreateDto;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Invoice
 * @property string due_date,
 * @property integer client_id,
 * @property integer company_id,
 * @property string|null description
 */

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'due_date' => 'required|string',
            'client_id' => 'required|integer',
            'company_id' => 'required',
            'invoice_items' => 'required',
            'description' => 'string',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $message = $validator->errors()->all();
        $error  = collect($message)->unique()->first();
        throw new HttpResponseException(
            response()->json(['status' => 'error', 'data' => $message, 'message' => $error], 422)
        );
    }

    public function convertToDto(): CreateDto
    {
        return new CreateDto(
            $this->due_date,
            $this->client_id,
            $this->company_id,
            $this->invoice_items,
            $this->description
        );
    }
}
