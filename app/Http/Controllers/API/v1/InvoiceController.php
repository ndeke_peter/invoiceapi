<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Helpers\ApiResponse;
use App\Services\InvoiceService;
use App\Http\Requests\Invoice\CreateRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    protected InvoiceService $invoiceService;
    
    public function __construct(
        InvoiceService $invoiceService
    ) {
        $this->invoiceService = $invoiceService;
    }

    /**
     * retrieve all resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allInvoice = $this->invoiceService->getAllInvoice();

        return ApiResponse::responseSuccess($allInvoice, "All Invoice");
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request): JsonResponse
    {
        try {
            $dto = $request->convertToDto();
            $invoice = $this->invoiceService->store(
                request()->user(),
                $dto
            );

            return ApiResponse::responseCreated($invoice->toArray(), "New Invoice created");
        } catch (\Exception $e) {
            return ApiResponse::responseException($e, 400, $e->getMessage());
        }
    }

}
