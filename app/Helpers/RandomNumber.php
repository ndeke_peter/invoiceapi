<?php

namespace App\Helpers;

class RandomNumber
{
    // Generate Standard Payment Transaction ID
    public static function generateTransactionRef()
    {
        //generate most suitable transaction ref
        $ref = date("Ymd") . time() . mt_rand(10000, 99999);
        return $ref;
    }
}
