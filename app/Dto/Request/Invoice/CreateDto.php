<?php
namespace App\Dto\Request\Invoice;

class CreateDto
{
    public string $due_date;
    public int $client_id;
    public int $company_id;
    public array $invoice_items;
    public ?string $description;
    

    public function __construct(
        string $due_date,
        int $client_id,
        int $company_id,
        array $invoice_items,
        ?string $description
    ) {
        $this->due_date = $due_date;
        $this->client_id = $client_id;
        $this->company_id = $company_id;
        $this->invoice_items = $invoice_items;
        $this->description = $description;
    }
}
