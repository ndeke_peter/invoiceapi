<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = ['invoiceItemCount'];
    protected $with = ['company', 'client', 'invoice_items'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function invoice_items()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function getInvoiceItemCountAttribute()
    {
        return $this->invoice_items()->count();
    }

    public function getTotalInvoiceItemAmountAttribute()
    {
        return $this->invoice_items()->sum('amount');
    }
}
