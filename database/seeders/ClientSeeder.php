<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Client::create([
          'fullname' => 'John Dew',
          'email' => 'johndew@gmail.com',
          'address' => '10, Downson, Lagos',
          'phone'  => '080324567890'
        ]);

        Client::create([
            'fullname' => 'Karl Marz',
            'email' => 'karlmarz@gmail.com',
            'address' => '12, Downson, Lagos',
            'phone'  => '080324567870'
        ]);
    }
}
