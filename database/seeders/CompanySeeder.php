<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'company_name' => 'Julius Berger',
            'company_email' => 'info@juliusberger.com',
            'company_address' => 'KM 10, Industrial Layout, Lagos',
            'company_phone' => '+234 80 324 5678',
            'contact_person' => 'Gbenga Ademola'
        ]);
    }
}
