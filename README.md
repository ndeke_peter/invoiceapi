# Invoice API (PHP/Laravel)

## Overview
- Handles the generation of invoice from a business to it's Clients.


## API Documentation
- https://documenter.getpostman.com/view/4857405/UV5f6DFu

## Requirements
- PHP 7.4 and above.
- Composer.


## Setting up locally for testing

- Clone project.
- copy .env.example to .env
- run composer install
- change the db credentials to your own in the .env and ensure the db was created in mysql
- run php artisan migrate --seed
- run php artisan serve
- project is ready for testing and will be available on http://123.0.0.1:8000
- login from the login endpoint using email: admin@admin.com and password: admin1234
